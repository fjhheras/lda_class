Cosmetic adaptation of the Linear Discriminant Analysis of Sebastian Raschka, published here:

https://sebastianraschka.com/Articles/2014_python_lda.html
